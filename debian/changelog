serp (1.15.1-2) unstable; urgency=medium

  * Team upload.
  * Drop dependency on cdbs.
  * Remove myself from uploaders. (Closes: #871885)
  * Bump DH compat level to 13.
  * Update Vcs-* fields.
  * Bump Standards-Version to 4.6.2. No changes were required.
  * Wrap and sort dependencies lists.
  * Update copyright info.
  * Apply Multi-Arch hints.
  * Bump watch file to version 4 and update upstream URL to track.
  * Drop obsolete get-orig-source target in d/rules.
  * Add field Rules-Requires-Root with 'no' as value.

 -- Miguel Landaeta <nomadium@debian.org>  Sat, 02 Dec 2023 13:11:28 +0000

serp (1.15.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream release
    - Removed compile-java-1.6.diff (applied upstream)
  * Standards-Version updated to 3.9.6 (no changes)
  * Use XZ compression for the upstream tarball
  * Switch to debhelper level 9

 -- Emmanuel Bourg <ebourg@apache.org>  Fri, 29 May 2015 17:26:27 +0200

serp (1.14.1-2) unstable; urgency=low

  * Switched from Ant to Maven build to publish pom.
  * Updated Standards-Version to 3.9.4.
  * Use canonical form of Vcs-* fields.
  * Added Stephen Nelson to list of uploaders.
  * Updated copyright to 1.0 format.
  * Updated copyright license to BSD-3-clause from BSD.
  * Added patch to generate Java 6 compatible bytecode.

 -- Stephen Nelson <stephen@eccostudio.com>  Tue, 27 Aug 2013 22:35:31 +0100

serp (1.14.1-1) unstable; urgency=low

  * Initial release. (Closes: #582782).

 -- Miguel Landaeta <miguel@miguel.cc>  Sun, 23 May 2010 12:47:32 -0430
